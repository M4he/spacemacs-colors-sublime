# Spacemacs color scheme for Sublime Text

This is a port from the original [Spacemacs color scheme](https://github.com/nashamri/spacemacs-theme) for Sublime Text (dark version only). The theme was generated using the [TmTheme-Editor](https://github.com/aziz/tmTheme-Editor) mainly. Also includes a matching theme for the [PlainTasks plugin](https://github.com/aziz/PlainTasks) that I created by hand.

**Please note that this is not an identical reproduction of Spacemacs' syntax highlighting!** Spacemacs and Sublime Text differ in how they handle syntax highlighting. This color scheme simply takes the original Spacemacs' colors and applies them to the most appropriate counterparts in Sublime Text.

I also chose to swap the purple-ish blue color used for variables (which hardly differed from the pure blue color used for keywords) with a dull yellow tone taken from the Spacemacs palette. See below screenshots for a preview.

## Screenshots

![Sample Screenshot (Python)](screenshots/python.png)

![Sample Screenshot (SimpleTasks)](screenshots/simpletasks.png)

(Font used in the Screenshots is "Roboto Mono")

## Thanks

Thanks goes to:

- [nashamri](https://github.com/nashamri) for creating the original [Spacemacs theme](https://github.com/nashamri/spacemacs-theme)
- [aziz](https://github.com/aziz) for creating/maintaining the [TmTheme-Editor](https://github.com/aziz/tmTheme-Editor)